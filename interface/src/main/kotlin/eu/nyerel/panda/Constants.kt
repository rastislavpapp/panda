package eu.nyerel.panda

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
object Constants {

    const val DUMP_FILE_NAME = "panda-profiler-dump-file.ser"

}
