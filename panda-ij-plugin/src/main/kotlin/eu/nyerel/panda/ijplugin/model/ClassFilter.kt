package eu.nyerel.panda.ijplugin.model

class ClassFilter(val patterns: List<String>) {

    companion object {

        fun fromArray(patterns: Array<String?>?): ClassFilter {
            return if (patterns == null) {
                ClassFilter(emptyList())
            } else {
                ClassFilter(patterns.filterNotNull().toList())
            }
        }

        fun fromFilters(filters: Array<com.intellij.ui.classFilter.ClassFilter>): ClassFilter {
            return ClassFilter(filters.map { it.pattern })
        }
    }

    override fun toString(): String {
        return patterns.joinToString(",")
    }

    fun toDisplayableString(): String {
        return patterns.joinToString(",\n")
    }

    fun toArray() = patterns.toTypedArray()

    fun toFilters() = patterns.map { com.intellij.ui.classFilter.ClassFilter(it) }.toTypedArray()

}