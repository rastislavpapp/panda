package eu.nyerel.panda.ijplugin.data

import com.intellij.openapi.diagnostic.Logger
import eu.nyerel.panda.Constants
import eu.nyerel.panda.monitoringresult.calltree.CallTreeList
import eu.nyerel.panda.monitoringresult.calltree.CallTreeNode

import java.io.*

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
object DumpFileReader {

    private val LOG = Logger.getInstance(DumpFileReader::class.java)

    private val tempDir: File = File(System.getProperty("java.io.tmpdir"))
    private val dumpFile: File = File(tempDir, Constants.DUMP_FILE_NAME)

    fun read(): List<CallTreeNode> {
        if (!dumpFile.exists()) {
            return emptyList()
        }
        FileInputStream(dumpFile).use {
            val input = ObjectInputStream(BufferedInputStream(it))
            return (input.readObject() as CallTreeList).nodes
        }
    }

    fun clear() {
        if (dumpFile.exists()) {
            if (!dumpFile.delete()) {
                LOG.warn("Failed to delete profiler dump file " + dumpFile.absolutePath)
            }
        }
    }

}
