package eu.nyerel.panda.ijplugin.runner.calltree

import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.ProgressManager
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.Project
import com.intellij.ui.treeStructure.treetable.TreeTable
import eu.nyerel.panda.ijplugin.data.AgentFacade
import eu.nyerel.panda.ijplugin.data.DumpFileReader
import eu.nyerel.panda.ijplugin.runner.PandaSettings
import eu.nyerel.panda.ijplugin.runner.calltree.model.AggregatedCallTreeTableModel
import eu.nyerel.panda.ijplugin.runner.calltree.model.CallTreeTableModel
import eu.nyerel.panda.ijplugin.runner.calltree.model.EmptyCallTreeModel
import eu.nyerel.panda.ijplugin.runner.calltree.model.WidthAware
import eu.nyerel.panda.ijplugin.runner.calltree.support.CallTreeNodeUtils
import eu.nyerel.panda.monitoringresult.calltree.CallTreeNode

import javax.swing.*

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
object CallTreeDrawer {

    lateinit var table: TreeTable

    fun drawInBackground(project: Project?) {
        ProgressManager.getInstance().run(
                object : Task.Backgroundable(project, "Updating Call Tree", false) {
                    override fun run(indicator: ProgressIndicator) {
                        indicator.isIndeterminate = true
                        draw()
                    }
                }
        )
    }

    fun draw() {
        var nodes: List<CallTreeNode> = loadCallTreeNodes()
        nodes = applyNodeViewSettings(nodes)
        drawNodes(nodes)
    }

    private fun applyNodeViewSettings(nodes: List<CallTreeNode>): List<CallTreeNode> {
        var result: List<CallTreeNode> = nodes
        if (PandaSettings.hideProxyClasses) {
            result = CallTreeNodeUtils.exclude(result, CallTreeNodeUtils.PROXY_CLASS_FILTER)
        }
        result = if (PandaSettings.aggregateCallTree) {
            if (PandaSettings.callTreeStructured) {
                CallTreeNodeUtils.aggregate(result)
            } else {
                CallTreeNodeUtils.flatAggregate(result)
            }
        } else {
            CallTreeNodeUtils.flatten(result)
        }
        if (PandaSettings.sortByDuration) {
            result = CallTreeNodeUtils.sortByDuration(result)
        }
        return result
    }

    private fun loadCallTreeNodes() = if (AgentFacade.isRunning()) {
        AgentFacade.getCallTree()
    } else {
        DumpFileReader.read()
    }

    private fun drawNodes(nodes: List<CallTreeNode>) {
        if (nodes.isEmpty()) {
            table.setModel(EmptyCallTreeModel())
            table.setRootVisible(true)
        } else {
            val model: CallTreeTableModel = if (PandaSettings.aggregateCallTree) {
                AggregatedCallTreeTableModel(nodes)
            } else {
                CallTreeTableModel(nodes)
            }
            SwingUtilities.invokeLater {
                table.setModel(model)
                table.setRootVisible(false)
                resizeColumns(table)
            }
        }
    }

    private fun resizeColumns(table: TreeTable) {
        val model = table.tableModel as CallTreeTableModel
        for (i in 0 until model.columnCount) {
            val column = table.columnModel.getColumn(i)
            val info = model.columnInfos[i]
            if (info is WidthAware) {
                column.preferredWidth = (info as WidthAware).preferredWidth
            }
        }
    }

}
