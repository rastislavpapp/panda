package eu.nyerel.panda.ijplugin.runner.calltree

import com.intellij.openapi.actionSystem.ActionGroup
import com.intellij.openapi.actionSystem.ActionManager
import com.intellij.openapi.actionSystem.ActionToolbar
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.SimpleToolWindowPanel
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowFactory
import com.intellij.ui.AnActionButton
import com.intellij.ui.dualView.TreeTableView
import eu.nyerel.panda.ijplugin.runner.calltree.model.EmptyCallTreeModel
import java.awt.BorderLayout

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
class PandaCallTreeWindowFactory : ToolWindowFactory {

    companion object {
        const val PANDA_TOOL_WINDOW_ID = "Panda"
    }

    override fun createToolWindowContent(project: Project, toolWindow: ToolWindow) {
        val cm = toolWindow.contentManager
        val factory = cm.factory
        val content = factory.createContent(CallTreeWindow(), "", false)
        cm.addContent(content)
    }

    class CallTreeWindow: SimpleToolWindowPanel(true, false) {

        private val toolbar: ActionToolbar = run {
            val actionManager = ActionManager.getInstance()
            val actionGroup = actionManager.getAction("CallTreeToolWindowGroup") as ActionGroup
            val toolbar = actionManager.createActionToolbar("CallTreeToolWindowToolbar", actionGroup, true)
            val actions = toolbar.actions
            for (action in actions.filterIsInstance<AnActionButton>()) {
                action.contextComponent = callTreeTable
            }
            toolbar.setTargetComponent(this)
            this.add(toolbar.component, BorderLayout.NORTH)
            toolbar
        }

        private val callTreeTable: TreeTableView = run {
            val model = EmptyCallTreeModel()
            val table = TreeTableView(model)
            CallTreeDrawer.table = table
            this.add(table)
            table
        }

    }

}