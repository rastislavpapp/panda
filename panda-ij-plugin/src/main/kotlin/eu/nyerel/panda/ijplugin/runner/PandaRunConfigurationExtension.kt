package eu.nyerel.panda.ijplugin.runner

import com.intellij.execution.RunConfigurationExtension
import com.intellij.execution.configurations.JavaParameters
import com.intellij.execution.configurations.RunConfigurationBase
import com.intellij.execution.configurations.RunnerSettings
import com.intellij.openapi.options.SettingsEditor
import org.jdom.Element

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
class PandaRunConfigurationExtension : RunConfigurationExtension() {

    override fun <P : RunConfigurationBase<*>> createEditor(configuration: P): SettingsEditor<P> {
        return PandaSettingsEditor(configuration.project)
    }

    override fun <T : RunConfigurationBase<*>?> updateJavaParameters(configuration: T, params: JavaParameters, runnerSettings: RunnerSettings?) {

    }

    override fun readExternal(configuration: RunConfigurationBase<*>, element: Element) {

    }

    override fun writeExternal(configuration: RunConfigurationBase<*>, element: Element) {

    }

    override fun getEditorTitle(): String {
        return "Panda"
    }

    override fun isEnabledFor(configuration: RunConfigurationBase<*>, runnerSettings: RunnerSettings?): Boolean {
        return true
    }

    override fun isApplicableFor(configuration: RunConfigurationBase<*>): Boolean {
        return true
    }

}