package eu.nyerel.panda.ijplugin.runner

import com.intellij.ide.util.PropertiesComponent
import com.intellij.openapi.project.Project
import eu.nyerel.panda.ijplugin.model.ClassFilter

private const val PROPERTY_PANDA_MONITORED_CLASSES = "panda.monitored.classes"
private const val PROPERTY_PANDA_EXCLUDED_CLASSES = "panda.excluded.classes"

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
object PandaSettings {

    var aggregateCallTree = true
    var callTreeStructured = true
    var sortByDuration = false
    var showPackageName = false
    var hideProxyClasses = true

    fun getMonitoredClasses(project: Project): ClassFilter {
        return getClasses(project, PROPERTY_PANDA_MONITORED_CLASSES)
    }

    fun getExcludedClasses(project: Project): ClassFilter {
        return getClasses(project, PROPERTY_PANDA_EXCLUDED_CLASSES)
    }

    private fun getClasses(project: Project, propertyName: String): ClassFilter {
        val properties = PropertiesComponent.getInstance(project)
        val classesProperty: Array<String?>? = properties.getValues(propertyName)
        return ClassFilter.fromArray(classesProperty)
    }

    fun setMonitoredClasses(project: Project, monitoredClasses: ClassFilter) {
        val properties = PropertiesComponent.getInstance(project)
        properties.setValues(PROPERTY_PANDA_MONITORED_CLASSES, monitoredClasses.toArray())
    }

    fun setExcludedClasses(project: Project, excludedClasses: ClassFilter) {
        val properties = PropertiesComponent.getInstance(project)
        properties.setValues(PROPERTY_PANDA_EXCLUDED_CLASSES, excludedClasses.toArray())
    }

}
