package eu.nyerel.panda.ijplugin

import com.intellij.openapi.application.PreloadingActivity
import com.intellij.openapi.progress.ProgressIndicator
import eu.nyerel.panda.ijplugin.data.DumpFileReader

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
class PandaPluginInitializer: PreloadingActivity() {

    override fun preload(indicator: ProgressIndicator) {
        DumpFileReader.clear()
    }

}