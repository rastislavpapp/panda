package eu.nyerel.panda.ijplugin.runner

import java.io.*

/**
 * @author Rastislav Papp (rastislav.papp@gmail.com)
 */
object JarUtils {

    private const val PANDA_AGENT_JAR_FILE_NAME = "panda-agent.jar"

    private val tempDir: File
        get() = File(System.getProperty("java.io.tmpdir"))

    fun unpackPandaAgentJar() = getUnpackedResourcePath("/$PANDA_AGENT_JAR_FILE_NAME")

    private fun getUnpackedResourcePath(resourcePath: String): String {
        val inputStream = JarUtils::class.java.getResourceAsStream(resourcePath)
                ?: throw IllegalArgumentException("Resource not found on path $resourcePath")
        inputStream.use {
            val tempFile = getAsTempFile(inputStream)
            return tempFile.absolutePath
        }
    }

    private fun getAsTempFile(inputStream: InputStream): File {
        val tempFile = File(tempDir.absolutePath, PANDA_AGENT_JAR_FILE_NAME)
        if (!tempFile.isFile) {
            val success = tempFile.createNewFile()
            if (!success) {
                throw IOException("Unable to create file " + tempFile.absolutePath)
            }
        }
        FileOutputStream(tempFile).use {
            inputStream.copyTo(it)
        }
        return tempFile
    }

}